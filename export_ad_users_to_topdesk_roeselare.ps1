# Script to export users to Topdesk Zorgbedrijf Roeselare
# --------------------------------------------------------
# created by driesverlinden
# version 1.1 - 5 maart 2020
# --------------------------------------------------------

# parameters
$ous = # FILL IN OU OR OUS TO FILTER ON IN AD, EXAMPLE FOR ONE OU: 'OU=Users,DC=wingene,DC=be' - EXAMPLE FOR TWO OUS: 'OU=Users,DC=wingene,DC=be','OU=OCMW,DC=wingene,DC=be'
$Filter = "(Enabled -eq 'True')" # filter on active user accounts
$City = # FILL IN NAME OF CITY TO CREATE SEPERATE CSV FILE, EXAMPLE: "wingene"
$LocationCSV = # FILL IN LOCATION OF EXPORT CSV FILE, ENDING \ REQUIRED, EXAMPLE: "E:\export\"

# import AD Module for Powershell
# Remark: installation of feature on running server: Active Directory module for Windows PowerShell
# Feature: Remote Server Administration Tools\Role Administration Tools\AD DS and AD LDS Tools\Active Directory module for Windows PowerShell
import-module activedirectory

# get AD objects for given OU's
$ous | 
ForEach { Get-ADUser -Filter "$Filter" -SearchBase $_ -Properties * } | 

# sort AD objects on GiveName
Sort-Object GivenName |

# create user friendly names for Properties
Select-Object @{Label = "Distinguished Name";Expression = {$_.DistinguishedName}},
    @{Label = "First Name";Expression = {$_.GivenName}},  
    @{Label = "Last Name";Expression = {$_.Surname}},             
    @{Label = "Logon Name";Expression = {$_.sAMAccountName}}, 
    @{Label = "Job Title";Expression = {$_.Title}}, 
    @{Label = "Department";Expression = {$_.Department}}, 
    @{Label = "Phone";Expression = {$_.telephoneNumber}}, 
    @{Label = "Email";Expression = {$_.Mail}},
    @{Label = "Manager";Expression = {%{(Get-AdUser $_.Manager -server $ADServer -Properties DistinguishedName).DistinguishedName}}} |

# export all info to csv
export-csv -Path "$LocationCSV gebruikers-$City.csv" -NoTypeInformation -Delimiter ';'