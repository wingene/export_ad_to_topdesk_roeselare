# Powershell script to send csv files to ftp-server

# default location of WinSCP
& "C:\Program Files (x86)\WinSCP\WinSCP.com" `
  /log="#LOCATION OF LOG FILES#.log" /ini=nul `
  /command `
    "open ftpes://#USERNAME#:#PASSWORD#@#FTPSERVER#/ -rawsettings CacheDirectories=0 CacheDirectoryChanges=0" `
    "put #FULL LOCATION CSV FILES#\gebruikers-#CITY#.csv" `
    "put #FULL LOCATION CSV FILES#\behandelaars-#CITY#.csv" `
    "exit"

$winscpResult = $LastExitCode
if ($winscpResult -eq 0)
{
  Write-Host "Success"
}
else
{
  Write-Host "Error"
}

exit $winscpResult