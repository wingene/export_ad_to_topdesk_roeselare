# README #

In this repository are two Powershell scripts to export information out of an Active Directory to a CSV file. 

Version 1.1 - 5 maart 2020

Last changes: 
- possible to change name of city, to create seperate csv-files per city

## How to send CSV file(s) to TOPdesk Roeselare? ##
With the free program WinSCP is it possible to create an automated script that sends the csv file(s) to a ftp server. 

### Can I use Task Scheduler with WINSCP? ###
Yes. At a new task and give the following action: 

Location of WinSCP installation on server, example: "C:\Program Files\WinSCP\WinSCP.exe"
Parameters: 
/log=$LOCATION$\winscp.log /command "open ftpes://$USERNAME$:$PASSWORD$@$SERVER$/" "put $FULL LOCATION gebruikers CSV$" "put $FULL LOCATION behandelaars CSV$" "exit"