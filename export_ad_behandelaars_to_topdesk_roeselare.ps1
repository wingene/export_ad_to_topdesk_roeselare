# Script to export behandelaars to Topdesk Zorgbedrijf Roeselare
# --------------------------------------------------------
# created by driesverlinden
# version 1.1 - 5 maart 2020
# --------------------------------------------------------

# parameters
$ADGroup = # FILL IN NAME OF AD GROUP WITH BEHANDELAARS FOR TOPDESK, EXAMPLE "TOPDESK-BEHANDELAARS"
$Filter = "(Enabled -eq 'True')" # use only active user accounts
$City = # FILL IN NAME OF CITY TO MAKE SEPERATE FILE, EXAMPLE "wingene"
$LocationCSV = # FILL IN LOCATION OF EXPORT CSV FILE - ENDING \ REQUIRED, EXAMPLE "E:\export\"

# import AD Module for Powershell
# Remark: installation of feature on running server: Active Directory module for Windows PowerShell
# Feature: Remote Server Administration Tools\Role Administration Tools\AD DS and AD LDS Tools\Active Directory module for Windows PowerShell
import-module activedirectory

# get AD objects for given OU's
Get-ADGroupMember $ADGroup |
Sort-Object GivenName |

Get-ADUser -Properties * |
Sort-Object GiveName |
# create user friendly names for Properties
Select-Object   @{Label = "Distinguished Name";Expression = {$_.DistinguishedName}},
    @{Label = "First Name";Expression = {$_.GivenName}},  
    @{Label = "Last Name";Expression = {$_.Surname}},             
    @{Label = "Logon Name";Expression = {$_.sAMAccountName}}, 
    @{Label = "Job Title";Expression = {$_.Title}}, 
    @{Label = "Department";Expression = {$_.Department}}, 
    @{Label = "Phone";Expression = {$_.telephoneNumber}}, 
    @{Label = "Email";Expression = {$_.Mail}},
    @{Label = "Manager";Expression = {%{(Get-AdUser $_.Manager -Properties DistinguishedName).DistinguishedName}}}  | 

# export all info to csv
export-csv -Path "$LocationCSV behandelaars-$City.csv" -NoTypeInformation -Delimiter ';'